package main

import "fmt"

func swap(array []int, i int, j int) {
	temp := array[i]
	array[i] = array[j]
	array[j] = temp
}

//noinspection GoNilContainerIndexing
func insertSort(array []int) []int {
	for i := 1; i < len(array); i++ {

		for j := i; j > 0; j-- {

			if array[j-1] > array[j] {
				swap(array, j, j-1)
			}
			//fmt.Printf("%v\n", array)

		}
	}

	return array
}

func bubbleSort(array []int) []int {
	done := false

	for !done {

		done = bubbleUp(array)
		//fmt.Printf("------------\n")
	}

	return array
}

func bubbleUp(array []int) (done bool) {
	done = true
	for j := 0; j < len(array)-1; j++ {

		currentElement := array[j]
		nextElement := array[j+1]
		//fmt.Printf("%v\n", array)
		//fmt.Printf("CURRENT: %v\n", currentElement)
		//fmt.Printf("NEXT:    %v\n", nextElement)

		if currentElement > nextElement {
			swap(array, j, j+1)
			// If any swaps have been done the algorithm is not done
			// It's only done if the inner loop finishes without going into this if statement
			done = false
		} else if j == len(array)-1 {
			j = 0
		}
	}

	return done
}

func quickSort(array []int) []int {
	if len(array) <= 1 {
		return array
	} else if len(array) == 2 {
		return bubbleSort(array)
	} else {

		left, right := hoarePartition(array)
		quickSort(left)
		quickSort(right)

		return array

	}
}

func lumotoPartition(array []int) (left []int, right []int) {
	fmt.Printf("%v\n", array)

	pivot := array[len(array)/2]

	fmt.Printf("PIVOT: %v\n\n", pivot)

	//for

	return
}

func hoarePartition(array []int) (left []int, right []int) {

	fmt.Printf("%v\n", array)

	pivot := array[len(array)/2]

	fmt.Printf("PIVOT: %v\n\n", pivot)

	leftIndex := 0
	rightIndex := len(array) - 1

	for true {

		for array[leftIndex] <= pivot {
			if leftIndex+1 < len(array) {
				leftIndex++
				fmt.Printf("LEFT: %v\n", leftIndex)
			} else {
				break
			}
		}

		for array[rightIndex-1] > pivot { // Try changing > to >= if it fails
			if rightIndex-1 > 0 {
				rightIndex--
				fmt.Printf("RIGHT: %v\n", rightIndex)
			} else {
				break
			}
		}

		if leftIndex >= rightIndex {
			return array[:rightIndex], array[rightIndex:]
		} else {
			swap(array, leftIndex, rightIndex-1)
			fmt.Printf("%v\n", array)
			fmt.Printf("\n")
		}
	}

	return
}

//----------------------------------------------------------------------------------------------------------------------
// UTILITIES
//----------------------------------------------------------------------------------------------------------------------

func cloneArray(unsortedArray []int) []int {
	array := make([]int, len(unsortedArray))
	copy(array, unsortedArray)
	return array
}

func sort(unsortedArray []int) (sortedArray []int) {
	array := cloneArray(unsortedArray)
	//return insertSort(array)
	//return bubbleSort(array)
	return quickSort(array)
}
