# Implementation of sorting algorithms in Go

Currently implemented algorithms are:

* Insert sort
* Bubble sort
* Quick sort

There elso exists a simple set of tests to verify the correct behaviour of each sorting algorithm. These have been written as an arrray of structs to make sure the code is as readable as possible.