package main

import (
	"math/rand"
	"reflect"
	"testing"
)

func Test(t *testing.T) {
	tests := []struct {
		input          []int
		expectedResult []int
	}{
		{[]int{7},
			[]int{7}},

		{[]int{8, 3},
			[]int{3, 8}},

		{[]int{1, 2, 3},
			[]int{1, 2, 3}},

		{[]int{3, 4, 8},
			[]int{3, 4, 8}},

		{[]int{3, 8, 4},
			[]int{3, 4, 8}},

		{[]int{4, 8, 3},
			[]int{3, 4, 8}},

		{[]int{8, 4, 3},
			[]int{3, 4, 8}},

		{[]int{8, 3, 4},
			[]int{3, 4, 8}},

		{[]int{4, 3, 8},
			[]int{3, 4, 8}},

		{[]int{4, 3, 8, 1234, 54, 6, 7, 6, 42, 3, 3, 6, 8, 9},
			[]int{3, 3, 3, 4, 6, 6, 6, 7, 8, 8, 9, 42, 54, 1234}},
	}
	for _, test := range tests {
		t.Run("Table Test", func(t *testing.T) {
			result := sort(test.input)
			//fmt.Printf("Input: %v\n", test.input)
			//fmt.Printf("Expected output: %v\n", test.expectedResult)

			if !reflect.DeepEqual(result, test.expectedResult) {
				t.Errorf("Expected %v but got %v instead!\n", test.expectedResult, result)
			}
		})
	}

}

func BenchmarkWorstCase(b *testing.B) {
	array := generateReverseArray(200)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		sort(array)
	}
}

func BenchmarkAverageCase(b *testing.B) {
	array := generateRandomArray(200)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		sort(array)
	}
}

func BenchmarkBestCase(b *testing.B) {
	array := generateSortedArray(200)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		sort(array)
	}
}

func generateReverseArray(length int) []int {
	array := make([]int, length)

	for i := 0; i < length; i++ {

		array[i] = length - i

	}

	return array
}

func generateRandomArray(length int) []int {
	array := make([]int, length)

	for i := 0; i < length; i++ {

		array[i] = rand.Int()

	}

	return array
}

func generateSortedArray(length int) []int {
	array := make([]int, length)

	for i := 0; i < length; i++ {

		array[i] = i

	}

	return array
}
